# OpenApi service

Для работы со всеми методами API Вам необходимо передавать в заголовке запросa X-Authorization специальный ключ доступа (secret). Он представляет собой строку из латинских букв и цифр и принадлежит виртуальному пользователю, созданному администратором в интерфейсе приложения TrainingSpace.

Поля *_percent возвращают значение от 0 до 100.

Все поля datetime возвращаются в формате iso8601 (2019-02-21T15:03:49+03:00)

## Оглавление

### REST API V1
  - [Пользователи](./api_v1/users.md)
  - [Программы](./api_v1/programs.md)
  - [Рекомендации](./api_v1/recommendations.md)

### REST API V2
  - [Пользователи](./api_v2/users.md)
  - [Программы](./api_v2/programs.md)
  - Статистика
    - [Юниты](./api_v2/statistics/units.md)
    - [Тесты](./api_v2/statistics/tests.md)
    - [Опросы](./api_v2/statistics/questionnaires.md)
  - Юниты
    - [Юниты](./api_v2/units/units.md)
    - [Aquariums](./api_v2/units/aquariums.md)
    - [Brainstorms](./api_v2/units/brainstorms.md)
    - [Cases](./api_v2/units/cases.md)
    - [Collaborations](./api_v2/units/collaborations.md)
    - [Documents](./api_v2/units/documents.md)
    - [Longreads](./api_v2/units/longreads.md)
    - [Minilections](./api_v2/units/minilections.md)
    - [Moderations](./api_v2/units/moderations.md)
    - [Problems](./api_v2/units/problems.md)
    - [Questionnaires](./api_v2/units/questionnaires.md)
    - [RoleGames](./api_v2/units/role_games.md)
    - [Tests](./api_v2/units/tests.md)
  - [Слоты](./api_v2/slots.md)
  - [Webhooks](./api_v2/webhooks.md)
  - [Сессии](./api_v2/sessions.md)
  - [LRS](./api_v2/lrs.md)
