# API методы версии 1, связанные с пользователями

## Список методов:

- ### Создать пользователя

URL: _/v1/users_

Method: POST

Content-Type: application/json

Description: создаем пользователя

Body:
```
{
  *email: string,
  *fname: string,
  *lname: string,
  *is_url_authentication: boolean,
  *gender: string,
  avatar_url: string,
  city: string,
  description: string,
  mname: string,
  phone: string,
  personal_number: string
}
```

Response:
```
{
  status: 200,
  body: {
    id: string,
    avatar_url: string,
    city: string,
    description: string,
    email: string,
    fname: string,
    gender: string,
    lname: string,
    mname: string,
    phone: string,
    is_url_authentication: boolean,
    authentication_url: string,
    personal_number: string
  }
},
{
  status: 422,
  body: {
    error: "Ошибка валидации данных: <ошибка>"
  }
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```
