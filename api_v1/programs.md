# API методы версии 1, связанные с программами

Возможные значения unit type:

value | description
--|--
text_doc  |  Текстовый документ (doc, pdf, ppt)
excel_doc  |  Табличный документ (xls)
presentation_doc  |  Презентация (ppt)
image  |  Изображение (фотослайд, фотогрид)
archive  |  Архив (zip)
video  |  Видео файл (mp4)
audio  |  Аудио файл (mp3)
longread  |  Лонгрид
link  |  Внешняя ссылка
questionnaire  |  Опрос
feedback_form  |  Анкета обратной связи
test  |  Тестирование
minilection  |  Слайд минилекции
brainstorm  |  Мозговой штурм
rolegame  |  Ролевая игра
aquarium  |  Аквариум
moderation  |  Модерация
case  |  Кейс
check_list  |  Чек-лист
course  |  Электронный курс (cmi5, xAPI, SCORM)

Возможные значения block type:

value | description
--|--
training  |  Тренинг
handout  |
microlearning  |

## Список методов:

- ### Получить все программы

URL: _/v1/programs_

Method: GET

Content-Type: application/json

Description: возвращает все версии программ

Response:
```
{
  code: 200,
  body: [
    {
      id: string,
      creator_id: string,
      title: string,
      description: string,
      background_image_url: string,
      blocks: [
        {
          id: string,
          title: string,
          position: integer,
          type: string,
          units: [
            {
              id: string,
              title: string,
              description: string,
              position: integer,
              type: string
            }
          ]
        }
      ]
    }
  ]
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```

- ### Получить программы пользователя, на которые он подписался

URL: _/v1/users/:id/programs_

Method: GET

Content-Type: application/json

Description: Возвращает программы, на которые подписался пользователь.

Response:
```
{
  code: 200,
  body: [
    {
      id: string,
      creator_id: string,
      title: string,
      description: string,
      background_image_url: string,
      completition_percent: integer,
      is_completed: boolean,
      blocks: [
        {
          id: string,
          title: string,
          position: integer,
          completition_percent: integer,
          is_completed: boolean,
          type: string,
          units: [
            {
              id: string,
              title: string,
              description: string,
              completition_percent: integer,
              is_completed: boolean,
              type: string
            }
          ],
          session: {
            {
              id: string,
              start_at: datetime,
              end_at: datetime
            }
          }
        }
      ]
    }
  ]
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```
