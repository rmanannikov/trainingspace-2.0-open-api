# API методы версии 1, связанные с подписками на сессии

## Список методов:

- ### Создать рекомендацию

URL: _/v1/users/:id/recommendations_

Method: POST

Content-Type: application/json

Description: Порекомендовать программу пользователю

Body:
```
{
  *program_id: string
}
```

Response:
```
{
  id: string,
  user_id: string,
  program_id: string
},
{
  status: 403,
  body: {
    error: "Организация пользователя не соответствует организации программы"
  }
},
{
  status: 422,
  body: {
    error: "Ошибка валидации данных: <ошибка>"
  }
},
{
  status: 404,
  body: {
    error: "Программа не найдена"
  }
},
},
{
  status: 404,
  body: {
    error: "Пользователь не найден"
  }
},
{
  status: 401,
  body: {
    error: "Пользователь не авторизирован"
  }
}
```

- ### Получить рекомендации пользователя

URL: _/v1/users/:id/recommendations_

Method: GET

Content-Type: application/json

Description: возвращает рекомендации

Response:
```
{
  code: 200,
  body: [
    {
      id: string,
      program_id: string
    }
  ]
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```
