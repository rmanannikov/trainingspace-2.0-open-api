# API методы версии 2, связанные с слотами

## Список методов:

- ### Создать слот

URL: _/v2/slots_

Method: POST

Content-Type: application/json

Description: Создать слот и подписки на него

Body:
```
{
  program_id: string,
  sessions: [
    {
      start_at: datetime,
      end_at: datetime,
      slot_id: string,
      master_ids: [string],
      block_version_id: string,
      subscription_user_ids: [string]
    }
  ]
}
```

Response:
```
{
  status: 201,
  body: {
    id: string,
    program_id: string,
    sessions: [
      {
        id: string,
        start_at: datetime,
        end_at: datetime,
        master_ids: [string],
        block_version_id: string
      }
    ],
    user_ids: [string]
  }
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```
