# API методы версии 2, связанные с программами

Возможные значения unit type:

value | description
--|--
text_doc  |  Текстовый документ (doc, pdf, ppt)
excel_doc  |  Табличный документ (xls)
presentation_doc  |  Презентация (ppt)
image  |  Изображение (фотослайд, фотогрид)
archive  |  Архив (zip)
video  |  Видео файл (mp4)
audio  |  Аудио файл (mp3)
longread  |  Лонгрид
link  |  Внешняя ссылка
questionnaire  |  Опрос
feedback_form  |  Анкета обратной связи
test  |  Тестирование
minilection  |  Слайд минилекции
brainstorm  |  Мозговой штурм
rolegame  |  Ролевая игра
aquarium  |  Аквариум
moderation  |  Модерация
case  |  Кейс
check_list  |  Чек-лист
course  |  Электронный курс (cmi5, xAPI, SCORM)

Возможные значения block type:

value | description
--|--
training  |  Тренинг
handout  |
microlearning  |

## Список методов:

- ### Получить версии программ пользователя, на которые он подписался

URL: _/v2/users/:id/program_versions/_

Method: GET

Content-Type: application/json

Description: Возвращает версии программ с результатом прохождения, на которые подписался пользователь.

Response:
```
{
  code: 200,
  body: [
    {
      id: string,
      program: {
        id: string,
        creator_id: string
      },
      recommendation_id: string,
      title: string,
      description: string,
      background_image_url: string,
      deadline: integer,
      is_started: boolean,
      is_completed: boolean,
      is_passed: boolean,
      score: integer,
      custom_data: jsonb,
      block_versions: [
        {
          id: string,
          block_id: string,
          title: string,
          position: integer,
          deadline: integer,
          type: string,
          is_started: boolean,
          is_completed: boolean,
          is_passed: boolean,
          score: integer,
          custom_data: jsonb,
          unit_versions: [
            {
              id: string,
              unit_id: string,
              title: string,
              description: string,
              deadline: integer,
              type: string,
              is_started: boolean,
              is_completed: boolean,
              is_passed: boolean,
              score: integer,
              custom_data: jsonb
            }
          ],
          session: {
            {
              id: string,
              start_at: datetime,
              end_at: datetime
            }
          }
        }
      ]
    }
  ]
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```
