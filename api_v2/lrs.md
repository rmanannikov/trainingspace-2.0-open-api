# API методы версии 2, связанные с lrs

## Список методов:

- ### Получить course link

URL: _/v2/course_link/_

Method: POST

Content-Type: application/json

Description:

Body:
```
{
  user_id: string,
  material_id: string,
  course_type: string,
  url: string,
  session_id: string
}
```

Response:
```
{
  code: 200,
  body: [
    {
      url: string
    }
  ]
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```
