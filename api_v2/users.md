# API методы версии 2, связанные с пользователями

В поле roles[] могут быть:
- student
- trainer
- methodologist
- admin
- supervisor
- learning_manager
- manager
- trainer_manager

custom_data - поле формата json, в которое записываются пользовательские данные в формате ключ:значение, например:
```
{
  "applicant_id": "123"
}
```

## Список методов:

### Создать пользователя

URL: _/v2/users_

Method: POST

Content-Type: application/json

Description: Создать пользователя, поля:
- custom_data: поле в которое записываются дополнительные данные о пользователе в соответствии с потребностями интегрированного приложения

Body:
```
{
  avatar_url: string,
  city: string,
  description: string,
  email: string,
  fname: string,
  gender: string,
  lname: string,
  mname: string,
  phone: string,
  position: string,
  confirmed_at: datetime,
  roles: [string],
  identification_number: string,
  leader_ids: [string],
  custom_data: jsonb,
  password: string
}
```

Response:
```
{
  status: 200,
  body: {
    id: string,
    avatar_url: string,
    city: string,
    description: string,
    email: string,
    fname: string,
    gender: string,
    lname: string,
    mname: string,
    phone: string,
    position: string,
    confirmed_at: datetime,
    roles: [string],
    identification_number: string,
    leader_ids: [string],
    custom_data: jsonb
  }
},
{
  status: 400,
  body: {
    error: "Ошибка валидации данных: <ошибка>"
  }
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```

### Обновить пользователя

URL: _/v2/users/external/:identificationnumber_

Method: PUT

Content-Type: application/json

Description: Обновить пользователя по идентификационному номеру, поля:
- custom_data: поле в которое записываются дополнительные данные о пользователе в соответствии с потребностями интегрированного приложения

Body:
```
{
  avatar_url: string,
  city: string,
  description: string,
  email: string,
  fname: string,
  gender: string,
  lname: string,
  mname: string,
  phone: string,
  position: string,
  confirmed_at: datetime,
  roles: [string],
  identification_number: string,
  leader_ids: [string],
  custom_data: jsonb
}
```

Response:
```
{
  status: 200,
  body: {
    id: string,
    avatar_url: string,
    city: string,
    description: string,
    email: string,
    fname: string,
    gender: string,
    lname: string,
    mname: string,
    phone: string,
    position: string,
    confirmed_at: datetime,
    roles: [string],
    identification_number: string,
    leader_ids: [string],
    custom_data: jsonb
  }
},
{
  status: 400,
  body: {
    error: "Ошибка валидации данных: <ошибка>"
  }
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```

### Обновить пользователя

URL: _/v2/users/:id_

Method: PUT

Content-Type: application/json

Description: Обновить пользователя, поля:
- custom_data: поле в которое записываются дополнительные данные о пользователе в соответствии с потребностями интегрированного приложения

Body:
```
{
  avatar_url: string,
  city: string,
  description: string,
  email: string,
  fname: string,
  gender: string,
  lname: string,
  mname: string,
  phone: string,
  position: string,
  confirmed_at: datetime,
  roles: [string],
  identification_number: string,
  leader_ids: [string],
  custom_data: jsonb
}
```

Response:
```
{
  status: 200,
  body: {
    id: string,
    avatar_url: string,
    city: string,
    description: string,
    email: string,
    fname: string,
    gender: string,
    lname: string,
    mname: string,
    phone: string,
    position: string,
    confirmed_at: datetime,
    roles: [string],
    identification_number: string,
    leader_ids: [string],
    custom_data: jsonb
  }
},
{
  status: 400,
  body: {
    error: "Ошибка валидации данных: <ошибка>"
  }
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```

### Получить информацию о пользователе

URL: _/v2/users/:id_

Method: GET

Content-Type: application/json

Description: Возвращает пользователя по id, поля:
- custom_data: поле в котором хранятся дополнительные данные о пользователе в соответствии с потребностями интегрированного приложения

Response:
```
{
  status: 200,
  body: {
    id: string,
    avatar_url: string,
    city: string,
    description: string,
    email: string,
    fname: string,
    gender: string,
    lname: string,
    mname: string,
    phone: string,
    position: string,
    confirmed_at: datetime,
    roles: [string],
    identification_number: string,
    leader_ids: [string],
    custom_data: jsonb
  }
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```

### Получить подчиненных сотрудника

URL: _/v2/leaders/:id/employees_

Method: GET

Content-Type: application/json

Description: Возвращает пользователя по id, если не указан nesting_level, то возвращаются подчиненные всех уровней

Query:
```
{
  nesting_level: integer
}
```

Response:
```
{
  status: 200,
  body: [
    {
      id: string,
      avatar_url: string,
      city: string,
      description: string,
      email: string,
      fname: string,
      gender: string,
      lname: string,
      mname: string,
      phone: string,
      position: string,
      confirmed_at: datetime,
      roles: [string],
      identification_number: string,
      leader_ids: [string]
    }
  ]
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```

### Получить сотрудников

URL: _/v2/users_

Method: GET

Content-Type: application/json

Description: Возвращает всех пользователей, запрос будет иметь вид: /v2/users?c[city]=Moscow&c[position]=Employee

Query:
```
{
  c = {
    field: value
  }
}
```

Response:
```
{
  status: 200,
  body: [
    {
      id: string,
      avatar_url: string,
      city: string,
      description: string,
      email: string,
      fname: string,
      gender: string,
      lname: string,
      mname: string,
      phone: string,
      position: string,
      confirmed_at: datetime,
      roles: [string],
      identification_number: string,
      leader_ids: [string]
    }
  ]
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```

- ### Смена пароля пользователя

URL: _/v2/change_password_

Method: PUT

Content-Type: application/json

Description: устанавливает новый пароль пользователя, пользователь берется из JWT

Body:
```
{
    old_password: string,
    password: string
}
```

Response:
```
{
    status: 200
    message: 'Ok',
},
{
    status: 400,
    message: "Old password is incorrect"
},
{
    status: 404
    message: 'user not found',
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```
