# API методы версии 2, связанные с сессиями

## Список методов:

- ### Создать сессию

URL: _/v2/sessions_

Method: POST

Content-Type: application/json

Description: Создаем сессию

Body:
```
{
  start_at: datetime,
  end_at: datetime,
  slot_id: string,
  master_ids: [string],
  block_version_id: string,
  subscription_user_ids: [string]
}
```

Response:
```
{
  status: 201,
  body: {
    id: string,
    start_at: datetime,
    end_at: datetime,
    master_ids: [string],
    block_version_id: string,
    /* если был запрос на создание подписок */
    subscriptions: [
      {
        id: string,
        user_id: string,
        subscription_status_id: string
      }
    ]
  }
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```
