# API методы версии 2, связанные с колбэками

## Список методов:

- ### Webhook при изменении статуса юнита

<!-- URL: _/v2/webhooks/unit_status_

Method: POST

Content-Type: application/json

Description: Создать подписку на изменение статуса версии юнита, в параметрах передается адрес колбэка и id юнита

Body:
```
{
  callback_url: string,
  unit_id: string
}
```

Response:
```
{
  status: 204
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
``` -->

Description fields:
- is_completed: статус корректного выполнения, выполнен (true)/не выполнен (false)
- is_passed: статус прохождения, пройден (true)/не пройден (false)
- score: счёт прохождения

Webhook Method: POST

Webhook Body:
```
{
  user_id: string,
  action: "unit_version_status_changed",
  unit_version_id: string,
  unit: {
    id: string,
    is_global: boolean
  },
  session_id: string,
  block_version_id: string,
  block_type_id: string,
  block_id: string,
  program_id: string,
  program_version_id: string,
  is_started: boolean,
  is_completed: boolean,
  is_passed: boolean,
  score: integer,
  recommendation_id: string,
  custom_data: jsonb
}
```

- ### Webhook при изменении статуса блока

<!-- URL: _/v2/webhooks/block_status_

Method: POST

Content-Type: application/json

Description: Создать подписку на изменение статуса версии блока, в параметрах передается адрес колбэка и id блока

Body:
```
{
  callback_url: string,
  block_id: string
}
```

Response:
```
{
  status: 204
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
``` -->

Description fields:
- is_completed: статус корректного выполнения, выполнен (true)/не выполнен (false)
- is_passed: статус прохождения, пройден (true)/не пройден (false)
- score: счёт прохождения

Webhook Method: POST

Webhook Body:
```
{
  user_id: string,
  action: "block_version_status_changed",
  block_version_id: string,
  block_type_id: string,
  block_id: string,
  session_id: string,
  program_version_id: string,
  program_id: string,
  is_started: boolean,
  is_completed: boolean,
  is_passed: boolean,
  score: integer,
  recommendation_id: string,
  custom_data: jsonb
}
```

- ### Webhook при изменении статуса программы

<!-- URL: _/v2/webhooks/program_status_

Method: POST

Content-Type: application/json

Description: Создать подписку на изменение статуса версии программы, в параметрах передается адрес колбэка и id программы

Body:
```
{
  callback_url: string,
  program_id: string
}
```

Response:
```
{
  status: 204
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
``` -->

Description fields:
- is_completed: статус корректного выполнения, выполнен (true)/не выполнен (false)
- is_passed: статус прохождения, пройден (true)/не пройден (false)
- score: счёт прохождения

Webhook Method: POST

Webhook Body:
```
{
  user_id: string,
  action: "program_version_status_changed",
  program_version_id: string,
  program_id: string,
  session_id: string,
  is_started: boolean,
  is_completed: boolean,
  is_passed: boolean,
  score: integer,
  recommendation_id: string,
  custom_data: jsonb
}
```
