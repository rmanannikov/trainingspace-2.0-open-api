# API методы версии 2, связанные с статистикой опросов

## Список методов:

- ### Создать результат scale questionnaire

URL: _/v2/questionnaire_scale_results_

Method: PATCH

Content-Type: application/json

Description: Создать/обновить результат scale questionnaire

Body:
```
{
  user_id: string,
  scale_id: string,
  value: integer,
  session_id: string
}
```

Response:
```
{
  status: 200,
  body: {
    id: integer,
    user_id: string,
    value: integer,
    session_id: string,
    scale_id: string
  }
},
{
  status: 403,
  body: {
    error: "Организация пользователя не соответствует организации юнита"
  }
},
{
  status: 400,
  body: {
    error: "Ошибка валидации данных: <ошибка>"
  }
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```
