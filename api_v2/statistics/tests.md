# API методы версии 2, связанные с статистикой тестов

## Список методов:

- ### Создать результат answer test

URL: _/v2/test_answer_results_

Method: POST

Content-Type: application/json

Description: Создать/обновить результат answer test

Body:
```
{
  user_id: string,
  answer_id: string,
  session_id: string
}
```

Response:
```
{
  status: 200,
  body: [
    {
      id: integer,
      user_id: string,
      session_id: string,
      answer_id: string
    }
  ]
},
{
  status: 403,
  body: {
    error: "Организация пользователя не соответствует организации юнита"
  }
},
{
  status: 400,
  body: {
    error: "Ошибка валидации данных: <ошибка>"
  }
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```
