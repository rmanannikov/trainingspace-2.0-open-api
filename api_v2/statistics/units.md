# API методы версии 2, связанные с статистикой юнитов

## Список методов:

- ### Получить результаты версии юнита

URL: _/v2/unit_versions/:id/results_

Method: GET

Content-Type: application/json

Description: Получить результаты версии юнита

Response:
```
{
  status: 200,
  body: {
    id: string,
    unit_id: string,
    unitable_id: string,
    unitable_type: string,
    results: [
      {
        id: integer,
        version_id: string,
        user_id: string,
        session_id: string,
        mark: integer,
        learning_time: integer,
        completion_percent: integer,
        is_completed: boolean,
        passing_percent: integer
      }
    ]
  }
},
{
  status: 404,
  body: {
    error: "Версия юнита не найдена"
  }
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```

- ### Сохранить результат версии юнита

URL: _/v2/unit_versions/:id/results_

Method: PATCH

Content-Type: application/json

Description: Создать/обновить результат версии юнита

Body:
```
{
  user_id: string,
  session_id: string,
  mark: integer,
  learning_time: integer,
  completion_percent: integer
}
```

Response:
```
{
  status: 200,
  body: {
    id: string,
    version_id: string,
    user_id: string,
    session_id: string,
    mark: integer,
    learning_time: integer,
    completion_percent: integer,
    is_completed: boolean,
    passing_percent: integer,
    is_passed: boolean
  }
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```
