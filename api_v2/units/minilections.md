# API методы версии 2, связанные с программными minilection и их версиями

## Список методов:

- ### Создать Minilection

URL: _/v2/minilections_

Method: POST

Content-Type: application/json

Description: Создать minilection

Body:
```
{
  session_id: string,
  block_type_id: string,
  minilection: {
    position: integer,
    block_group_id: string,
    title: string,
    description: string,
    delay: integer,
    delay_time: time,
    delay_message: string,
    slide_versions: [
      {
        name: string,
        slide_type_id: string,
        image_url: string,
        list: array,
        ration: float,
        order: boolean,
        participant_underliner: string,
        trainer_underliner: string
      }
    ],
    activity: {
      participant_underliner: string,
      trainer_underliner: string
    }
  }
}
```

Response:
```
{
  status: 201,
  body: {
    id: string, // id из training_block
    /* if training_block */
    reference_id: string,
    block_version_id: string,
    unit_type_id: string,
    position: integer,
    is_global: boolean,
    delay: integer,
    delay_time: time,
    delay_message: string,
    block_group: {
      id: string,
      name: string
    },
    /* end */
    session_id: string,
    unit: {
      id: string,
      is_global: boolean
    },
    version: integer,
    is_draft: boolean,
    title: string,
    description: string,
    relations: [
      {
        id: string,
        block_version_id: string,
        block_group_id: string
      }
    ],
    activity: {
      id: string,
      participant_underliner: string,
      trainer_underliner: string
    },
    unitable: {
      id: string,
      type: string,
      slide_versions: [
        {
          id: string,
          name: string,
          slide_type_id: string,
          slide_id: string,
          image_url: string,
          list: array,
          ration: float,
          order: boolean,
          participant_underliner: string,
          trainer_underliner: string
        }
      ]
    }
  }
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```
