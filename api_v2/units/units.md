# API методы версии 2, связанные с юнитами

## Список методов:

- ### Получить версии юнитов конкретной сессии

URL: _/v2/unit_versions_

Method: GET

Content-Type: application/json

Description: возвращает юнит версии конкретной сессии

Query:
```
{
  session_id: string
}
```

Response:
```
{
  code: 200,
  body: [
    {
      id: string,
      unit: {
        id: string,
        is_global: boolean
      },
      title: string,
      description: string,
      activity: {
        id: string,
        participant_underliner: string,
        trainer_underliner: string,
        group: {
          id: string,
          is_grouping_by_count: boolean,
          group_count: integer,
          group_size: integer
        }
      },
      unitable: {
        id: string,
        type: string,
        /* if unitable_type == Collaboration::Collaboration */
        game_duration: integer,
        is_waiting_for_members: boolean,
        image_url: string,
        /* if unitable_type == Minilection::Minilection */
        slide_versions: [
          {
            id: string,
            name: string,
            slide_type_id: string,
            slide_id: string,
            image_url: string,
            list: array,
            ration: float,
            order: boolean,
            participant_underliner: string,
            trainer_underliner: string
          }
        ],
        /* if unitable_type == Aquarium::Aquarium */
        duration: integer,
        check_items: [
          {
            id: string,
            name: string,
            comment: string
          }
        ],
        roles: [
          {
            id: string,
            role_type: string,
            description: string
          }
        ],
        /* if unitable_type == Brainstorm::Brainstorm */
        is_only_trainer_ideas: boolean,
        is_skipping_vote: boolean,
        max_votes: integer,
        top_ideas_count: integer,
        generation_time: integer,
        is_skipping_group_vote: boolean,
        max_group_votes: integer,
        top_group_ideas_count: integer,
        /* if unitable_type == Problem::Problem */
        max_problems: integer,
        max_problem_votes: integer,
        max_solution_votes: integer,
        problem_generation_time: integer,
        problem_voting_time: integer,
        solution_generation_time: integer,
        solution_voting_time: integer,
        is_waiting_for_members: boolean,
        /* if unitable_type == RoleGame::RoleGame */
        is_waiting_for_members: boolean,
        is_rotate_roles: boolean,
        preparation_time: integer,
        game_duration: integer,
        feedback_time: integer,
        role_versions: [
          {
            id: string,
            name: string,
            description: string,
            role_type_id: string,
            role_id: string,
            bullets: [
              {
                id: string,
                name: string,
                round: integer
              }
            ]
          }
        ],
        /* if unitable_type == Moderation::Moderation */
        main_column: string,
        is_only_trainer_ideas: boolean,
        generation_time: integer,
        is_skipping_group_vote: boolean,
        max_group_votes: integer,
        top_group_ideas_count: integer,
        /* if unitable_type == Document::Document */
        file_url: string,
        /* if unitable_type == Longread::Longread */
        item_versions: [
          {
            id: string,
            position: integer,
            value: {
                /* if item_type value is text */
                text: string,
                /* if item_type value is image */
                file_url: string,
                /* if item_type value is video */
                file_url: string
            },
            item_type_id: string,
            item_id: string
          }
        ],
        /* if unitable_type == Questionnaire::Questionnaire */
        question_versions: [
          {
            id: string,
            position: integer,
            question_id: string,
            questionable: {
              id: string,
              type: string,
              /* if questionable_type == Questionnaire::Scale */
              min_value: integer,
              max_value: integer,
              title: string,
              subtitle: string,
              image_url: string,
              /* if questionable_type == Questionnaire::Text */
              title: string,
              subtitle: string,
              image_url: string,
              /* if questionable_type == Questionnaire::Survey */
              title: string,
              subtitle: string,
              image_url: string,
              survey_type_id: string,
              answers: [
                {
                  id: string,
                  value: string
                }
              ]
            }
          }
        ],
        /* if unitable_type == Case::Case */
        item_versions: [
          {
            id: uuid,
            position: integer,
            item_id: string,
            itemable: {
              id: string,
              type: string,
              /* if itemable_type == Case::Video */
              file_url: string,
              /* if itemable_type == Case::Image */
              file_url: string,
              /* if itemable_type == Case::Text */
              value: string
            }
          }
        ],
        /* if unitable_type == Test::Test */
        question_versions: [
          {
            id: string,
            position: integer,
            question_id: string,
            question_type_id: string,
            text: string,
            answers: [
              {
                id: string,
                value: string,
                is_correct: boolean
              }
            ]
          }
        ]
      },
      version: integer,
      is_draft: boolean,
      relations: [
        {
          id: string,
          block_version_id: string,
          block_group_id: string
        }
      ]
    }
  }
],
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```

- ### Получить версию юнита

URL: _/v2/unit_versions/:id_

Method: GET

Content-Type: application/json

Description: возвращает материал конкретной версии

Response:
```
{
  code: 200,
  body: {
    id: string,
    unit: {
      id: string,
      is_global: boolean
    },
    title: string,
    description: string,
    activity: {
      id: string,
      participant_underliner: string,
      trainer_underliner: string,
      group: {
        id: string,
        is_grouping_by_count: boolean,
        group_count: integer,
        group_size: integer
      }
    },
    unitable: {
      id: string,
      type: string,
      /* if unitable_type == Collaboration::Collaboration */
      game_duration: integer,
      is_waiting_for_members: boolean,
      image_url: string,
      /* if unitable_type == Minilection::Minilection */
      slide_versions: [
        {
          id: string,
          name: string,
          slide_type_id: string,
          slide_id: string,
          image_url: string,
          list: array,
          ration: float,
          order: boolean,
          participant_underliner: string,
          trainer_underliner: string
        }
      ],
      /* if unitable_type == Aquarium::Aquarium */
      duration: integer,
      check_items: [
        {
          id: string,
          name: string,
          comment: string
        }
      ],
      roles: [
        {
          id: string,
          role_type: string,
          description: string
        }
      ],
      /* if unitable_type == Brainstorm::Brainstorm */
      is_only_trainer_ideas: boolean,
      is_skipping_vote: boolean,
      max_votes: integer,
      top_ideas_count: integer,
      generation_time: integer,
      is_skipping_group_vote: boolean,
      max_group_votes: integer,
      top_group_ideas_count: integer,
      /* if unitable_type == Problem::Problem */
      max_problems: integer,
      max_problem_votes: integer,
      max_solution_votes: integer,
      problem_generation_time: integer,
      problem_voting_time: integer,
      solution_generation_time: integer,
      solution_voting_time: integer,
      is_waiting_for_members: boolean,
      /* if unitable_type == RoleGame::RoleGame */
      is_waiting_for_members: boolean,
      is_rotate_roles: boolean,
      preparation_time: integer,
      game_duration: integer,
      feedback_time: integer,
      role_versions: [
        {
          id: string,
          name: string,
          description: string,
          role_type_id: string,
          role_id: string,
          bullets: [
            {
              id: string,
              name: string,
              round: integer
            }
          ]
        }
      ],
      /* if unitable_type == Moderation::Moderation */
      main_column: string,
      is_only_trainer_ideas: boolean,
      generation_time: integer,
      is_skipping_group_vote: boolean,
      max_group_votes: integer,
      top_group_ideas_count: integer,
      /* if unitable_type == Document::Document */
      file_url: string,
      /* if unitable_type == Longread::Longread */
      item_versions: [
        {
          id: string,
          position: integer,
          value: {
              /* if item_type value is text */
              text: string,
              /* if item_type value is image */
              file_url: string,
              /* if item_type value is video */
              file_url: string
          },
          item_type_id: string,
          item_id: string
        }
      ],
      /* if unitable_type == Questionnaire::Questionnaire */
      question_versions: [
        {
          id: string,
          position: integer,
          question_id: string,
          questionable: {
            id: string,
            type: string,
            /* if questionable_type == Questionnaire::Scale */
            min_value: integer,
            max_value: integer,
            title: string,
            subtitle: string,
            image_url: string,
            /* if questionable_type == Questionnaire::Text */
            title: string,
            subtitle: string,
            image_url: string,
            /* if questionable_type == Questionnaire::Survey */
            title: string,
            subtitle: string,
            image_url: string,
            survey_type_id: string,
            answers: [
              {
                id: string,
                value: string
              }
            ]
          }
        }
      ],
      /* if unitable_type == Case::Case */
      item_versions: [
        {
          id: uuid,
          position: integer,
          item_id: string,
          itemable: {
            id: string,
            type: string,
            /* if itemable_type == Case::Video */
            file_url: string,
            /* if itemable_type == Case::Image */
            file_url: string,
            /* if itemable_type == Case::Text */
            value: string
          }
        }
      ],
      /* if unitable_type == Test::Test */
      question_versions: [
        {
          id: string,
          position: integer,
          question_id: string,
          question_type_id: string,
          text: string,
          answers: [
            {
              id: string,
              value: string,
              is_correct: boolean
            }
          ]
        }
      ]
    },
    version: integer,
    is_draft: boolean,
    relations: [
      {
        id: string,
        block_version_id: string,
        block_group_id: string
      }
    ]
  }
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```

- ### Обновить результаты версии юнита

URL: _/v2/unit_versions/:id/users/:id/results_

Method: POST

Content-Type: application/json

Description: обновить статус прохождения версии юнита сотрудником

Body:
```
{
  is_passed: boolean,
  is_completed: boolean
}
```

Response:
```
{
  code: 204
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```


- ### Создаьт версию юнита для сессии

URL: _/v2/unit_versions_

Method: POST

Content-Type: application/json

Description: Добавить уникальный для сессии юнит

Body:
```
{
  "session_id": string,
  "group_id": null,
  "type": "questionnaire",
  "title": string,
  "required": bool,
  "comments_disabled": bool,
  "block_type_id": number,
  "delay": number,
  "delay_time": string,
  "delay_message": string,
  "duration": number,
  "block_group_id": number,
  "can_reanswer": bool,
  "question_versions": [
    {
      "position": number,
      "questionable": {
        "type": "Questionnaire::Survey",
        "survey_type_id": 1,
        "answers": [
          {
            "id": null,
            "value": string,
            "position": number
          },
          {
            "id": null,
            "value": string,
            "position": number
          },
          {
            "id": null,
            "value": string,
            "position": number
          }
        ]
      },
      "title": string,
      "subtitle": string,
      "image_url": string
    },
    {
      "position": 1,
      "questionable": {
        "type": "Questionnaire::Survey",
        "survey_type_id": 2,
        "answers": [
          {
            "id": null,
            "value": string,
            "position": 0
          },
          {
            "id": null,
            "value": string,
            "position": 1
          },
          {
            "id": null,
            "value": string,
            "position": number
          },
          {
            "id": null,
            "value": string,
            "position": number
          }
        ]
      },
      "title": string,
      "subtitle": string,
      "image_url": string
    },
    {
      "position": number,
      "questionable": {
        "type": "Questionnaire::Text",
        "answers_count": number
      },
      "title": string,
      "subtitle": string,
      "image_url": string
    },
    {
      "position": number,
      "questionable": {
        "type": "Questionnaire::Scale",
        "min_value": string,
        "max_value": string,
        "use_for_rating": bool,
        "competency_id": number
      },
      "title": string,
      "subtitle": string,
      "image_url": string
    },
    {
      "position": number,
      "questionable": {
        "type": "Questionnaire::Brainstorm"
      },
      "title": string,
      "subtitle": string,
      "image_url": string
    }
  ]
}
```

Response:
```
{
  code: 204
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```
