# API методы версии 2, связанные с программными role_game и их версиями

В **каждом** запросе **должно быть**:

Cookie:
```
{
  JWT: string
}
```

## Список методов:

- ### Создать RoleGame

URL: _/v2/role_games_

Method: POST

Content-Type: application/json

Description: Создать role game

Body:
```
{
  session_id: string,
  block_type_id: string,
  role_game: {
    position: integer,
    block_group_id: string,
    title: string,
    description: string,
    delay: integer,
    delay_time: time,
    delay_message: string,
    is_waiting_for_members: boolean,
    is_rotate_roles: boolean,
    preparation_time: integer,
    game_duration: integer,
    feedback_time: integer,
    activity: {
      participant_underliner: string,
      trainer_underliner: string
    },
    role_versions: [
      {
        name: string,
        description: string,
        role_type_id: string,
        bullets: [
          {
            name: string,
            round: integer
          }
        ]
      }
    ]
  }
}
```

Response:
```
{
  status: 201,
  body: {
    id: string, // id из training_block
    /* if training_block */
    reference_id: string,
    block_version_id: string,
    unit_type_id: string,
    position: integer,
    is_global: boolean,
    delay: integer,
    delay_time: time,
    delay_message: string,
    block_group: {
      id: string,
      name: string
    },
    /* end */
    session_id: string,
    unit: {
      id: string,
      is_global: boolean
    },
    version: integer,
    is_draft: boolean,
    title: string,
    description: string,
    activity: {
      id: string,
      participant_underliner: string,
      trainer_underliner: string
    },
    unitable: {
      id: string,
      type: string,
      is_waiting_for_members: boolean,
      is_rotate_roles: boolean,
      preparation_time: integer,
      game_duration: integer,
      feedback_time: integer,
      role_versions: [
        {
          id: string,
          name: string,
          description: string,
          role_type_id: string,
          bullets: [
            {
              id: string,
              name: string,
              round: integer
            }
          ]
        }
      ]
    }
  }
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```
