# API методы версии 2, связанные с программными test и их версиями

В **каждом** запросе **должно быть**:

Cookie:
```
{
  JWT: string
}
```

## Список методов:

- ### Создать Test

URL: _/v2/tests_

Method: POST

Content-Type: application/json

Description: Создать test

Body:
```
{
  session_id: string,
  block_type_id: string,
  test: {
    position: integer,
    block_group_id: string,
    title: string,
    description: string,
    delay: integer,
    delay_time: time,
    delay_message: string,
    activity: {
      participant_underliner: string,
      trainer_underliner: string,
      group: {
        is_grouping_by_count: boolean,
        group_count: integer,
        group_size: integer
      }
    },
    question_versions: [
      {
        position: integer,
        text: string,
        question_type_id: string,
        answers: [
          {
            value: string,
            is_correct: boolean
          }
        ]
      }
    ]
  }
}
```

Response:
```
{
  status: 201,
  body: {
    id: string, // id из training_block
    /* if training_block */
    reference_id: string,
    block_version_id: string,
    unit_type_id: string,
    position: integer,
    is_global: boolean,
    delay: integer,
    delay_time: time,
    delay_message: string,
    block_group: {
      id: string,
      name: string
    },
    /* end */
    session_id: string,
    unit: {
      id: string,
      is_global: boolean
    },
    version: integer,
    is_draft: boolean,
    title: string,
    description: string,
    activity: {
      id: string,
      participant_underliner: string,
      trainer_underliner: string,
      group: {
        id: string,
        is_grouping_by_count: boolean,
        group_count: integer,
        group_size: integer
      }
    },
    unitable: {
      id: string,
      type: string,
      question_versions: [
        {
          id: string,
          position: integer,
          question_id: string,
          text: string,
          question_type_id: string,
          answers: [
            {
              id: string,
              value: string,
              is_correct: boolean
            }
          ]
        }
      ]
    }
  }
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```
