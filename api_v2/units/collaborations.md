# API методы версии 2, связанные с программными collaboration и их версиями

## Список методов:

- ### Создать Collaboration

URL: _/v2/collaborations_

Method: POST

Content-Type: application/json

Description: Создать collaboration

Body:
```
{
  session_id: string,
  block_type_id: string,
  minilection: {
    position: integer,
    block_group_id: string,
    title: string,
    description: string,
    delay: integer,
    delay_time: time,
    delay_message: string,
    game_duration: integer,
    is_waiting_for_members: boolean,
    image_url: string,
    activity: {
      participant_underliner: string,
      trainer_underliner: string
    }
  }
}
```

Response:
```
{
  status: 200,
  body: {
    id: string, // id из training_block
    /* if training_block */
    reference_id: string,
    block_version_id: string,
    unit_type_id: string,
    position: integer,
    is_global: boolean,
    delay: integer,
    delay_time: time,
    delay_message: string,
    block_group: {
      id: string,
      name: string
    },
    /* end */
    unit: {
      id: string,
      is_global: boolean
    },
    version: integer,
    is_draft: boolean,
    title: string,
    description: string,
    session_id: string,
    relations: [
      {
        id: string,
        block_version_id: string,
        block_group_id: string
      }
    ],
    activity: {
      id: string,
      participant_underliner: string,
      trainer_underliner: string
    },
    unitable: {
      id: string,
      type: string,
      game_duration: integer,
      is_waiting_for_members: boolean,
      image_url: string
    }
  }
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```
