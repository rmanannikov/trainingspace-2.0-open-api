# API методы версии 2, связанные с программными problems и их версиями

## Список методов:

- ### Создать Problem

URL: _/v2/problems_

Method: POST

Content-Type: application/json

Description: Создать problem

Body:
```
{
  session_id: string,
  block_type_id: string,
  problem: {
    position: integer,
    block_group_id: string,
    title: string,
    description: string,
    delay: integer,
    delay_time: time,
    delay_message: string,
    max_problems: integer,
    max_problem_votes: integer,
    max_solution_votes: integer,
    problem_generation_time: integer,
    problem_voting_time: integer,
    solution_generation_time: integer,
    solution_voting_time: integer,
    is_waiting_for_members: boolean,
    activity: {
      participant_underliner: string,
      trainer_underliner: string
    }
  }
}
```

Response:
```
{
  status: 201,
  body: {
    id: string, // id из training_block
    /* if training_block */
    reference_id: string,
    block_version_id: string,
    unit_type_id: string,
    position: integer,
    is_global: boolean,
    delay: integer,
    delay_time: time,
    delay_message: string,
    block_group: {
      id: string,
      name: string
    },
    /* end */
    session_id: string,
    unit: {
      id: string,
      is_global: boolean
    },
    version: integer,
    is_draft: boolean,
    title: string,
    description: string,
    relations: [
      {
        id: string,
        block_version_id: string,
        block_group_id: string
      }
    ],
    activity: {
      id: string,
      participant_underliner: string,
      trainer_underliner: string
    },
    unitable: {
      id: string,
      type: string,
      max_problems: integer,
      max_problem_votes: integer,
      max_solution_votes: integer,
      problem_generation_time: integer,
      problem_voting_time: integer,
      solution_generation_time: integer,
      solution_voting_time: integer,
      is_waiting_for_members: boolean
    }
  }
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```
