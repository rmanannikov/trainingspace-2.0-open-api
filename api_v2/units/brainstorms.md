# API методы версии 2, связанные с программными brainstorms и их версиями

## Список методов:

- ### Создать Brainstorm

URL: _/v2/brainstorms_

Method: POST

Content-Type: application/json

Description: Создать brainstorm

Body:
```
{
  session_id: string,
  block_type_id: string,
  brainstorm: {
    position: integer,
    block_group_id: string,
    title: string,
    description: string,
    delay: integer,
    delay_time: time,
    delay_message: string,
    is_only_trainer_ideas: boolean,
    is_skipping_vote: boolean,
    max_votes: integer,
    top_ideas_count: integer,
    generation_time: integer,
    is_skipping_group_vote: boolean,
    max_group_votes: integer,
    top_group_ideas_count: integer,
    activity: {
      participant_underliner: string,
      trainer_underliner: string,
      group: {
        is_grouping_by_count: boolean,
        group_count: integer,
        group_size: integer
      }
    }
  }
}
```

Response:
```
{
  status: 201,
  body: {
    id: string, // id из training_block
    /* if training_block */
    reference_id: string,
    block_version_id: string,
    unit_type_id: string,
    position: integer,
    is_global: boolean,
    delay: integer,
    delay_time: time,
    delay_message: string,
    block_group: {
      id: string,
      name: string
    },
    /* end */
    session_id: string,
    unit: {
      id: string,
      is_global: boolean
    },
    version: integer,
    is_draft: boolean,
    title: string,
    description: string,
    session_id: string,
    relations: [
      {
        id: string,
        block_version_id: string,
        block_group_id: string
      }
    ],
    unitable: {
      id: string,
      type: string,
      is_only_trainer_ideas: boolean,
      is_skipping_vote: boolean,
      max_votes: integer,
      top_ideas_count: integer,
      generation_time: integer,
      is_skipping_group_vote: boolean,
      max_group_votes: integer,
      top_group_ideas_count: integer,
      activity: {
        id: string,
        participant_underliner: string,
        trainer_underliner: string,
        group: {
          id: string,
          is_grouping_by_count: boolean,
          group_count: integer,
          group_size: integer
        }
      }
    }
  }
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```
