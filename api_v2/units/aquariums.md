# API методы версии 2, связанные с программными aquariums и их версиями

## Список методов:

- ### Создать Aquarium

URL: _/v2/aquariums_

Method: POST

Content-Type: application/json

Description: Создать aquarium

Body:
```
{
  session_id: string,
  block_type_id: string,
  aquarium: {
    position: integer,
    block_group_id: string,
    title: string,
    description: string,
    delay: integer,
    delay_time: time,
    delay_message: string,
    duration: integer,
    check_items: [
      {
        name: string,
        comment: string
      }
    ],
    roles: [
      {
        role_type: string,
        description: string
      }
    ],
    activity: {
      participant_underliner: string,
      trainer_underliner: string
    }
  }
}
```

Response:
```
{
  status: 201,
  body: {
    id: string, // id из training_block
    /* if training_block */
    reference_id: string,
    block_version_id: string,
    unit_type_id: string,
    position: integer,
    is_global: boolean,
    delay: integer,
    delay_time: time,
    delay_message: string,
    block_group: {
      id: string,
      name: string
    },
    /* end */
    unit: {
      id: string,
      is_global: boolean
    },
    version: integer,
    is_draft: boolean,
    title: string,
    session_id: string,
    description: string,
    relations: [
      {
        id: string,
        block_version_id: string,
        block_group_id: string
      }
    ],
    unitable: {
      id: string,
      type: string,
      duration: integer,
      check_items: [
        {
          id: string,
          name: string,
          comment: string
        }
      ],
      roles: [
        {
          id: string,
          role_type: string,
          description: string
        }
      ],
      activity: {
        id: string,
        participant_underliner: string,
        trainer_underliner: string
      }
    }
  }
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```
