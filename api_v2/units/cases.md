# API методы версии 2, связанные с программными case и их версиями

## Список методов:

- ### Создать Case

URL: _/v2/cases_

Method: POST

Content-Type: application/json

Description: Создать case

Body:
```
{
  session_id: string,
  block_type_id: string,
  case: {
    position: integer,
    block_group_id: string,
    title: string,
    description: string,
    delay: integer,
    delay_time: time,
    delay_message: string,
    item_versions: [
      {
        position: integer,
        itemable: {
          type: string,
          position: integer,
          /* if type == Case::Video */
          file_url: string,
          /* if type == Case::Image */
          file_url: string,
          /* if type == Case::Text */
          value: string
        }
      }
    ]
  }
}
```

Response:
```
{
  status: 201,
  body: {
    id: string, // id из training_block
    /* if training_block */
    reference_id: string,
    block_version_id: string,
    unit_type_id: string,
    position: integer,
    is_global: boolean,
    delay: integer,
    delay_time: time,
    delay_message: string,
    block_group: {
      id: string,
      name: string
    },
    /* end */
    unit: {
      id: string,
      is_global: boolean
    },
    version: integer,
    is_draft: boolean,
    title: string,
    description: string,
    session_id: string,
    unitable: {
      id: string,
      type: string,
      item_versions: [
        {
          id: string,
          position: integer,
          item_id: string,
          itemable: {
            id: string,
            type: string,
            position: integer,
            /* if type == Case::Video */
            file_url: string,
            /* if type == Case::Image */
            file_url: string,
            /* if type == Case::Text */
            value: string
          }
        }
      ]
    }
  }
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```
