# API методы версии 2, связанные с программными questionnaire и их версиями

## Список методов:

- ### Создать Questionnaire

URL: _/v2/questionnaires_

Method: POST

Content-Type: application/json

Description: Создать questionnaire

Body:
```
{
  session_id: string,
  block_type_id: string,
  questionnaire: {
    position: integer,
    block_group_id: string,
    title: string,
    description: string,
    delay: integer,
    delay_time: time,
    delay_message: string,
    activity: {
      participant_underliner: string,
      trainer_underliner: string,
      group: {
        is_grouping_by_count: boolean,
        group_count: integer,
        group_size: integer
      }
    },
    question_versions: [
      {
        position: integer,
        questionable: {
          type: string,
          position: integer,
          /* if type == Questionnaire::Scale */
          min_value: integer,
          max_value: integer,
          title: string,
          subtitle: string,
          image_url: string,
          /* if type == Questionnaire::Text */
          title: string,
          subtitle: string,
          image_url: string,
          /* if type == Questionnaire::Survey */
          title: string,
          subtitle: string,
          image_url: string,
          survey_type_id: string,
          answers: [
            {
              value: string
            }
          ]
        }
      }
    ]
  }
}
```

Response:
```
{
  status: 201,
  body: {
    id: string, // id из training_block
    /* if training_block */
    reference_id: string,
    block_version_id: string,
    unit_type_id: string,
    position: integer,
    is_global: boolean,
    delay: integer,
    delay_time: time,
    delay_message: string,
    block_group: {
      id: string,
      name: string
    },
    /* end */
    session_id: string,
    unit: {
      id: string,
      is_global: boolean
    },
    version: integer,
    is_draft: boolean,
    title: string,
    description: string,
    activity: {
      id: string,
      participant_underliner: string,
      trainer_underliner: string,
      group: {
        id: string,
        is_grouping_by_count: boolean,
        group_count: integer,
        group_size: integer
      }
    },
    unitable: {
      id: string,
      type: string,
      question_versions: [
        {
          id: string,
          position: integer,
          question_id: string,
          questionable: {
            id: string,
            type: string,
            position: integer,
            /* if type == Questionnaire::Scale */
            min_value: integer,
            max_value: integer,
            title: string,
            subtitle: string,
            image_url: string,
            /* if type == Questionnaire::Text */
            title: string,
            subtitle: string,
            image_url: string,
            /* if type == Questionnaire::Survey */
            title: string,
            subtitle: string,
            image_url: string,
            survey_type_id: string,
            answers: [
              {
                id: string,
                value: string
              }
            ]
          }
        }
      ]
    }
  }
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```
