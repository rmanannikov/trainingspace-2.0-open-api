# API методы версии 2, связанные с программными longread и их версиями

## Список методов:

- ### Создать Longread

URL: _/v2/longreads_

Method: POST

Content-Type: application/json

Description: Создать longread

Body:
```
{
  session_id: string,
  block_type_id: string,
  longread: {
    position: integer,
    block_group_id: string,
    title: string,
    description: string,
    delay: integer,
    delay_time: time,
    delay_message: string,
    item_versions: [
      {
        position: integer,
        value: {
            /* if item_type value is text */
            text: string,
            /* if item_type value is image */
            file_url: string,
            /* if item_type value is video */
            file_url: string
        },
        item_type_id: string
      }
    ]
  }
}
```

Response:
```
{
  status: 201,
  body: {
    id: string, // id из training_block
    /* if training_block */
    reference_id: string,
    block_version_id: string,
    unit_type_id: string,
    position: integer,
    is_global: boolean,
    delay: integer,
    delay_time: time,
    delay_message: string,
    block_group: {
      id: string,
      name: string
    },
    /* end */
    session_id: string,
    unit: {
      id: string,
      is_global: boolean
    },
    version: integer,
    is_draft: boolean,
    title: string,
    description: string,
    unitable: {
      id: string,
      type: string,
      item_versions: [
        {
          id: string,
          position: integer,
          value: {
              /* if item_type value is text */
              text: string,
              /* if item_type value is image */
              file_url: string,
              /* if item_type value is video */
              file_url: string
          },
          item_type_id: string,
          item_id: string
        }
      ]
    }
  }
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```
