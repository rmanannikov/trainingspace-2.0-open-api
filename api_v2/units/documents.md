# API методы версии 2, связанные с программными document и их версиями

## Список методов:

- ### Создать Document

URL: _/v2/documents_

Method: POST

Content-Type: application/json

Description: Создать document

Body:
```
{
  session_id: string,
  block_type_id: string,
  document: {
    position: integer,
    block_group_id: string,
    title: string,
    description: string,
    delay: integer,
    delay_time: time,
    delay_message: string,
    file_url: string
  }
}
```

Response:
```
{
  status: 201,
  body: {
    id: string, // id из training_block
    /* if training_block */
    reference_id: string,
    block_version_id: string,
    unit_type_id: string,
    position: integer,
    is_global: boolean,
    delay: integer,
    delay_time: time,
    delay_message: string,
    block_group: {
      id: string,
      name: string
    },
    /* end */
    session_id: string,
    unit: {
      id: string,
      is_global: boolean
    },
    version: integer,
    is_draft: boolean,
    title: string,
    description: string,
    unitable: {
      id: string,
      type: string,
      file_url: string
    }
  }
},
{
  status: 401,
  body: {
    error: "Неверный токен"
  }
}
```
